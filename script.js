// Data
let bricksCoord = [

  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 0,    y: 170,   width: 80, height: 30, label: 'HTML', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 160,  y: 170,   width: 80, height: 30, label: 'CSS', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 0,    y: 140,   width: 80, height: 30, label: 'PHP', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 160,  y: 140,   width: 80, height: 30, label: 'JS', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 0,    y: 110,   width: 80, height: 30, label: 'MySQL', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 80,   y: 110,   width: 80, height: 30, label: 'Mongo', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 160,  y: 110,   width: 80, height: 30, label: 'Node', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 0,    y: 80,    width: 80, height: 30, label: 'Java', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 80,   y: 80,    width: 80, height: 30, label: 'Angular', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 160,  y: 80,    width: 80, height: 30, label: 'git', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 0,    y: 50,    width: 80, height: 30, label: 'jQuery', textColor: "#54626f", color: "white"},
  {ix: Math.random()*(580)+10, iy: Math.random()*280+10, r: Math.random()*360, x: 160,  y: 50,    width: 80, height: 30, label: 'Symfony', textColor: "#54626f", color: "white"},

]

let doorCoord = [
  {ix: 100, iy: 100, r: Math.random()*360, x: 80,    y: 140,   width: 80, height: 60, label: '', textColor: "white", color: "#54626f"},
]

let roomCoords = [
  {id: "leftRoom", x: 5, y: 56, skewY: 0, dy: 0},
  {id: "leftRoom", x: 5, y: 56, skewY: 10, dy: -33},
  {id: "leftRoom", x: 5, y: 56, skewY: -20, dy: 68},
  // {id: "centerRoom", x: 200, y: 50},
  {id: "rightRoom", x: 425, y: 56, skewY: 0, dy: 0},
  {id: "rightRoom", x: 425, y: 56, skewY: -10, dy: 75},
  {id: "rightRoom", x: 425, y: 56, skewY: 20, dy: -155},
]


// Affichage temporisé des éléments
d3.select('#subtitle').transition().duration(3000).style('color', 'white')
d3.select('button').transition().delay(3000).duration(100).style('color', 'white').style('border-color', 'white')

let unusedBricks = d3.select('.canva').select('#background')
  .selectAll('rect')
  .data(bricksCoord)
  .enter()
  .append('rect')
  .transition()
  .delay(2000)
  .duration(2000)
  .attr("width", function(d){return d.width})
  .attr("height", function(d){return d.height})
  .attr("x", function(d){return d.ix })
  .attr("y", function(d){return d.iy })
  .attr("transform", function(d){return "rotate("+ d.r +", "+ d.ix +", "+ d.iy +")"})
  .attr("fill", function(d){return d.color})
  .attr("stroke", "#54626f")
  .attr("class", "bricks")


// Construction du chateau+
d3.select('#build').on('click', function() {
  d3.select('.canva').select('#background').selectAll('rect')
    .transition()
    .duration(1000)
    .attr("width", function(d){return d.width})
    .attr("height", function(d){return d.height})
    .attr("x", function(d){return d.x + 185})
    .attr("y", function(d){return d.y + 40})
    .attr("transform", "rotate(0, 0, 0)");
  d3.select('.canva').select('#background')
    .selectAll('text')
    .data(bricksCoord)
    .enter()
    .append('text')
    .attr("fill", function(d){return d.textColor})
    .attr("x", function(d){return d.x+ 40 + 185})
    .attr("y", function(d){return d.y + 20 + 40})
    .attr("text-anchor", "middle")
    .attr("font-size", "0.8em")
    .text(function(d){return d.label})
    .transition()
    .duration(1000);
  d3.select('.canva').select('#foreground')
    .append('text')
    .text("Entrez !")
    .attr('x', 305)
    .attr('y', 255)
    .attr("text-anchor", "middle")
    .attr("font-size", "0.8em")
    .attr("fill", "white")
    .attr('class', 'key')
  d3.select('.canva').select('#foreground')
    .append('text')
    .text("C'est ouvert à clé")
    .attr('x', 305)
    .attr('y', 270)
    .attr("text-anchor", "middle")
    .attr("font-size", "0.8em")
    .attr("fill", "white")
    .attr('class','key');
  d3.select('button').style('display', 'none')
  d3.select('.canva').select('#foreground').selectAll('rect')
    .data(doorCoord)
    .enter()
    .append('rect')
    .attr("id", "door")
    .attr("width", function(d){return d.width})
    .attr("height", function(d){return d.height})
    .attr("x", function(d){return d.x + 185})
    .attr("y", function(d){return d.y + 40})
    .attr("fill", "#ff0f67")
    .on('click', function(){
      //hide text
      d3.select('.canva')
        .selectAll('text')
        .text('');
      //flip open castle
      d3.select('.canva').select('#door')
        .transition()
        .duration(1000)
        .attr("fill", "white")
        .attr("transform", "translate (-610, -450) scale(3, 2.8) ")
        .attr("stroke", "#54626f")
      //remove bricks
      d3.select('.canva').select("#background")
        .selectAll('.bricks')
        .transition()
        .duration(1000)
        .attr("opacity", 0)
      //add rooms
      d3.select('.canva').select("#foreground")
        .selectAll('.rect')
        .data(roomCoords)
        .enter()
        .append('rect')
        .attr("fill", "white")
        .attr("stroke", "white")
        .attr("width", 180)
        .attr("height", 164)
        .attr("x", function(d){return d.x })
        .attr("y", function(d){return d.y })
        .attr("class", (d) => {return d.id + " room"})
      // // initialize room
      d3.select('.leftRoom')
        .attr('transform', 'translate(200, 0) scale(0, 1)')
      d3.select('.rightRoom')
        .attr('transform', 'translate(400, 0) scale(0, 1)')
      //animate room opening
      d3.selectAll('.room')
        .transition()
        .duration(1000)
        .attr('transform', (d) => { return 'skewY('+ d.skewY +') translate(0, '+ d.dy + ')'})
        .attr('opacity', 1)
  })


})
